-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 17, 2020 at 08:00 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blog_belajar`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `category` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id`, `title`, `content`, `created_at`, `updated_at`, `deleted_at`, `category`) VALUES
(1, '7 Makanan Tradisional Khas Jawa yang Cocok Dijadikan saat HUT RI Ke-75', 'Jakarta - Makanan tradisional khas Jawa pilihannya sangat beragam. Semua makanan ini memiliki makna spesial sehingga jadi sajian popular saat HUT RI yang ke-75 ini.', NULL, NULL, NULL, 'HUT RI Ke-75'),
(2, 'Test', 'Test', NULL, NULL, NULL, 'Test'),
(3, 'Dr.', 'Voluptatem corporis quia ut dolorem iure. Quasi quasi fuga qui architecto voluptates magni sed. Eligendi sit magni optio quia.', '2020-09-10 07:53:44', '2020-09-10 07:53:44', NULL, 'Actor'),
(4, 'Mr.', 'Consequatur iure accusantium dolorem et deserunt est illum blanditiis. Cumque est voluptatem dolore est quos nihil. Autem blanditiis minus molestiae odio voluptas odit adipisci.', '2020-09-10 07:53:44', '2020-09-10 07:53:44', NULL, 'Sys Admin'),
(5, 'Dr.', 'Tempore et dolores nulla quia deleniti odio vel. Id modi iure consectetur laboriosam sint est sequi fugiat. Velit consequatur repellat dolor deserunt.', '2020-09-10 07:53:45', '2020-09-10 07:53:45', NULL, 'Lodging Manager'),
(6, 'Prof.', 'Nam sed voluptatum explicabo quod. Qui iste quaerat occaecati beatae corrupti corporis. Officia quod nam qui minima qui quibusdam. Labore error et quaerat.', '2020-09-10 07:53:45', '2020-09-10 07:53:45', NULL, 'Nutritionist'),
(7, 'Mr.', 'Hic dolorem quia perspiciatis totam. Repudiandae tempora repudiandae odio possimus at corrupti illo. Labore temporibus sapiente voluptatem enim dolorum esse. Adipisci quasi sunt nemo distinctio nemo.', '2020-09-10 07:53:45', '2020-09-10 07:53:45', NULL, 'Tractor-Trailer Truck Driver'),
(8, 'Dr.', 'Id est eligendi et possimus iure nihil sed. Aut et ipsum quia sapiente enim nemo. Dolores asperiores cupiditate ut minima et.', '2020-09-10 07:53:45', '2020-09-10 07:53:45', NULL, 'Mold Maker'),
(9, 'Miss', 'Perferendis eveniet eum sed quia eligendi eveniet sapiente. Qui saepe aspernatur minima. Natus cupiditate est dolor sit et.', '2020-09-10 07:53:45', '2020-09-10 07:53:45', NULL, 'Meat Packer'),
(10, 'Prof.', 'Et dolores et est vero ducimus sunt. Eum maxime et natus omnis et expedita ex. Qui debitis ab maxime reiciendis accusantium quidem nihil.', '2020-09-10 07:53:45', '2020-09-10 07:53:45', NULL, 'Artillery Officer'),
(11, 'Mrs.', 'Soluta voluptas officiis qui earum totam voluptas et. Assumenda omnis saepe ipsa nisi. Odio sit doloribus ab et repudiandae numquam. Eos quis quibusdam earum dolorum.', '2020-09-10 07:53:45', '2020-09-10 07:53:45', NULL, 'Home'),
(12, 'Mrs.', 'Et deleniti quia velit. Quos minima nostrum molestias laboriosam. Ratione impedit dolorem corporis sit. Maxime aut doloremque sint minus maiores laboriosam ea.', '2020-09-10 07:53:45', '2020-09-10 07:53:45', NULL, 'Typesetting Machine Operator'),
(13, 'Ms.', 'Accusantium praesentium molestiae nulla sit a eos minima. Repudiandae alias molestiae expedita veniam debitis quia. Voluptas molestiae sed inventore quis cum.', '2020-09-10 07:53:45', '2020-09-10 07:53:45', NULL, 'Executive Secretary'),
(14, 'Dr.', 'Distinctio officia ab et officiis qui enim odit velit. Quasi amet est quia porro. Quibusdam explicabo dolore architecto iste.', '2020-09-10 07:53:46', '2020-09-10 07:53:46', NULL, 'HR Manager'),
(15, 'Mr.', 'Et quibusdam aut est facere. Labore tenetur nemo et explicabo. Perspiciatis ipsum ducimus magnam dicta.', '2020-09-10 07:53:46', '2020-09-10 07:53:46', NULL, 'Cabinetmaker'),
(16, 'Prof.', 'Placeat aut id ipsa dolor. Eius delectus unde minus adipisci est necessitatibus.', '2020-09-10 07:53:46', '2020-09-10 07:53:46', NULL, 'Gas Distribution Plant Operator'),
(17, 'Prof.', 'Autem ipsum molestiae debitis sint eius nam. Fugit qui sapiente expedita ipsam atque. Ut repellat consequuntur recusandae. Eos et dolore assumenda commodi earum sunt iusto recusandae.', '2020-09-10 07:53:46', '2020-09-10 07:53:46', NULL, 'Historian'),
(18, 'Dr.', 'Et libero quo dignissimos facilis. Dolorum suscipit porro veritatis a est nesciunt sunt. Dolorum optio excepturi est rerum deserunt. Sit asperiores voluptatem animi numquam et earum.', '2020-09-10 07:53:46', '2020-09-10 07:53:46', NULL, 'Manicurists'),
(19, 'Mr.', 'Doloribus animi in fuga impedit. Exercitationem voluptatum quam perspiciatis facere aut similique. Eos esse voluptatum illo beatae iste. Doloremque dolor inventore assumenda consectetur officiis.', '2020-09-10 07:53:46', '2020-09-10 07:53:46', NULL, 'Aircraft Cargo Handling Supervisor'),
(20, 'Miss', 'Ut nihil sint occaecati repellat quo illum quibusdam at. Libero dolorem qui quia qui sunt autem.', '2020-09-10 07:53:46', '2020-09-10 07:53:46', NULL, 'Employment Interviewer'),
(21, 'Prof.', 'Vel ipsa sint iste aliquam cum. In rerum veniam omnis ducimus iusto enim ut consequatur. Ducimus tenetur at blanditiis qui tempore quasi qui. Expedita aut a dolore ut quia voluptatem facere.', '2020-09-10 07:53:46', '2020-09-10 07:53:46', NULL, 'Fire-Prevention Engineer'),
(22, 'Mr.', 'Ullam doloribus laboriosam error architecto eum. Veniam non deserunt autem. Voluptatem harum enim itaque qui odit blanditiis aliquid cumque. Sint aut id amet.', '2020-09-10 07:53:46', '2020-09-10 07:53:46', NULL, 'Travel Clerk'),
(23, 'Prof.', 'Veritatis quam nulla cum omnis. Consequatur non nobis eveniet ut. Illum inventore sed corporis adipisci. Sed et soluta ut alias quas aut. Rerum labore atque explicabo ea ipsam voluptas.', '2020-09-10 07:53:46', '2020-09-10 07:53:46', NULL, 'Multiple Machine Tool Setter'),
(24, 'Prof.', 'Enim sint enim cumque non. Fugiat ullam explicabo placeat explicabo atque quia corrupti. Aut earum accusamus optio doloribus quod quisquam officiis. Voluptatem fugit labore incidunt nam sed. Eos autem repellat ea recusandae quidem et qui.', '2020-09-10 07:53:46', '2020-09-10 07:53:46', NULL, 'Driver-Sales Worker'),
(25, 'Dr.', 'Dignissimos non alias veniam aliquam nihil magni provident iusto. A mollitia cumque aut veritatis cum. Assumenda doloremque esse reiciendis voluptas possimus accusamus.', '2020-09-10 07:53:46', '2020-09-10 07:53:46', NULL, 'Health Practitioner'),
(26, 'Prof.', 'Dolorum itaque corrupti omnis ad assumenda minima fugiat eos. Quae nisi dolor dicta. Reiciendis explicabo eius deserunt non et explicabo molestias.', '2020-09-10 07:53:46', '2020-09-10 07:53:46', NULL, 'Fitter'),
(27, 'Dr.', 'Id ut ut alias est fugiat quia voluptas aut. Blanditiis et quis reiciendis soluta eum consequatur id atque. Iusto similique est magni sed. Quia laudantium labore praesentium perspiciatis officia ab aliquid.', '2020-09-10 07:53:46', '2020-09-10 07:53:46', NULL, 'Secondary School Teacher'),
(28, 'Prof.', 'Rerum et ducimus quia corporis earum qui. Eveniet veniam ea pariatur dolorem est ipsa. Ratione amet omnis mollitia quia.', '2020-09-10 07:53:47', '2020-09-10 07:53:47', NULL, 'Pharmacy Technician'),
(29, 'Prof.', 'Aut amet sit id voluptates magni quibusdam sit exercitationem. Aut aut qui est accusantium laboriosam voluptas ut.', '2020-09-10 07:53:47', '2020-09-10 07:53:47', NULL, 'Continuous Mining Machine Operator'),
(30, 'Miss', 'Accusamus voluptatem saepe debitis. Sed quos accusantium dolores velit doloremque nisi. Provident quis adipisci quidem modi.', '2020-09-10 07:53:47', '2020-09-10 07:53:47', NULL, 'Internist'),
(31, 'Prof.', 'Est qui est porro quo molestias. Quae dolore in dolores perspiciatis voluptatem. Qui enim ullam excepturi dicta. Sapiente voluptatem illo modi ea illum voluptas id iure.', '2020-09-10 07:53:47', '2020-09-10 07:53:47', NULL, 'Fabric Pressers'),
(32, 'Prof.', 'Et cumque rerum fugiat numquam aspernatur ex quis. Enim unde aliquid voluptatem quod. Ullam aperiam dolor sint.', '2020-09-10 07:53:47', '2020-09-10 07:53:47', NULL, 'Welder'),
(33, 'Miss', 'Cumque asperiores voluptatem eum. Consequatur et atque sed fugiat aut sit. Praesentium at est omnis commodi inventore. Saepe rem commodi autem nihil explicabo consequatur ad sunt.', '2020-09-10 07:53:47', '2020-09-10 07:53:47', NULL, 'Chemical Equipment Operator'),
(34, 'Miss', 'Maxime ut veniam voluptatem qui rerum animi laudantium. Necessitatibus id itaque eveniet omnis quo. Eius consequatur eveniet harum non qui voluptate. Et cum cum numquam id id.', '2020-09-10 07:53:47', '2020-09-10 07:53:47', NULL, 'Painter and Illustrator'),
(35, 'Ms.', 'Inventore sunt et dignissimos corrupti. Pariatur dolorem illo accusamus aspernatur deleniti qui. Soluta earum aut dolorem facilis praesentium et amet.', '2020-09-10 07:53:47', '2020-09-10 07:53:47', NULL, 'Anesthesiologist'),
(36, 'Prof.', 'Nulla numquam ratione consequatur laborum expedita distinctio eaque. Fugiat omnis quo optio qui pariatur sapiente et. Qui officia rerum possimus repudiandae ducimus assumenda.', '2020-09-10 07:53:47', '2020-09-10 07:53:47', NULL, 'Manufacturing Sales Representative'),
(37, 'Ms.', 'Ratione repellendus atque repellendus est veniam suscipit iusto. Et eveniet quam necessitatibus sequi. Ad ducimus vel nemo qui enim sit. Excepturi quo distinctio consequatur et.', '2020-09-10 07:53:47', '2020-09-10 07:53:47', NULL, 'Gas Processing Plant Operator'),
(38, 'Dr.', 'Ad vel ipsa quaerat est culpa. Mollitia doloribus maiores fugiat qui dicta id repellendus beatae. Earum omnis veniam quibusdam ad eum nihil recusandae ab. Culpa fugiat molestiae sit iure.', '2020-09-10 07:53:47', '2020-09-10 07:53:47', NULL, 'Telephone Operator'),
(39, 'Mrs.', 'Qui et vel sunt atque suscipit excepturi. Laboriosam quia quia sint esse. Sed libero culpa et occaecati cumque tenetur. Tenetur quibusdam eligendi illum sapiente.', '2020-09-10 07:53:47', '2020-09-10 07:53:47', NULL, 'Proofreaders and Copy Marker'),
(40, 'Mrs.', 'Doloremque velit at repellat voluptas. Necessitatibus et ut voluptatem dolorem. Accusantium ut aut consequuntur quod est mollitia.', '2020-09-10 07:53:48', '2020-09-10 07:53:48', NULL, 'Short Order Cook'),
(41, 'Mrs.', 'Facere vero quod vel. Repudiandae qui laborum voluptas nobis voluptatum accusamus. Eius amet explicabo et placeat earum enim.', '2020-09-10 07:53:48', '2020-09-10 07:53:48', NULL, 'Reservation Agent OR Transportation Ticket Agent'),
(42, 'Mr.', 'Quia blanditiis voluptates soluta eum eos voluptas. Porro aliquid id est odio et eum omnis culpa. Et quasi facere unde sint et est dolorem. Reprehenderit fuga ipsa ipsa facilis sit cum totam.', '2020-09-10 07:53:48', '2020-09-10 07:53:48', NULL, 'Employment Interviewer'),
(43, 'Dr.', 'Delectus quae inventore quia ab omnis. Architecto dolor laboriosam sunt reprehenderit minima repellat. Amet aperiam quo ut eum animi eos.', '2020-09-10 07:53:48', '2020-09-10 07:53:48', NULL, 'Mathematical Scientist'),
(44, 'Dr.', 'Consectetur nihil qui sed autem doloremque eaque eius. Corrupti est numquam ut. Adipisci a alias id autem possimus placeat minima. Tempore quis qui et est sint.', '2020-09-10 07:53:48', '2020-09-10 07:53:48', NULL, 'Mathematician'),
(45, 'Dr.', 'Minima impedit autem atque voluptatibus modi nihil dolores. Repellat laudantium et voluptas quia dolorem. Sunt tempora perspiciatis sapiente vero voluptas.', '2020-09-10 07:53:48', '2020-09-10 07:53:48', NULL, 'Emergency Medical Technician and Paramedic'),
(46, 'Mr.', 'Cum fugiat qui quasi est quasi debitis. Modi nihil enim quis cumque laborum sit totam saepe. Et veritatis tenetur est aut facere.', '2020-09-10 07:53:48', '2020-09-10 07:53:48', NULL, 'Animal Trainer'),
(47, 'Ms.', 'Ut soluta illo aliquid nobis. Voluptas et sed ad voluptas. Natus fugiat deleniti ipsa et reiciendis saepe provident. Quis ipsam voluptatem modi velit maxime vel et tenetur.', '2020-09-10 07:53:48', '2020-09-10 07:53:48', NULL, 'CTO'),
(48, 'Prof.', 'Nisi facilis praesentium itaque enim ex in non. Sequi sed eveniet ut quae ipsum debitis.', '2020-09-10 07:53:48', '2020-09-10 07:53:48', NULL, 'Electrical Drafter'),
(49, 'Dr.', 'Quas consequatur vitae est inventore ut commodi. Maxime qui ipsum animi repellat. Minus suscipit tempore quisquam distinctio quia laudantium.', '2020-09-10 07:53:48', '2020-09-10 07:53:48', NULL, 'Sewing Machine Operator'),
(50, 'Miss', 'Est placeat qui aut repudiandae dolor. Nam consequatur molestiae tempore eaque velit. Enim aliquid odit deserunt sed est magni sunt. Quo voluptatum repellendus in et. Sunt sed similique dolores libero et omnis vitae.', '2020-09-10 07:53:48', '2020-09-10 07:53:48', NULL, 'Sawing Machine Operator'),
(51, 'Mrs.', 'Aliquid eligendi id voluptatum quae aut omnis. Provident sit magnam quo rerum velit dolor aperiam ut. Recusandae labore quam fuga voluptatem minima ut. Cum perspiciatis est accusantium ipsum. Quia nulla aut optio voluptatem asperiores accusantium est.', '2020-09-10 07:53:48', '2020-09-10 07:53:48', NULL, 'Rough Carpenter'),
(52, 'Prof.', 'Molestias impedit tenetur beatae molestiae dolorem laborum. Aut quae quasi distinctio sapiente necessitatibus atque tempore sunt. Id consequuntur molestias exercitationem maiores. Quis dolores natus neque.', '2020-09-10 07:53:48', '2020-09-10 07:53:48', NULL, 'Bindery Machine Operator'),
(53, 'Prof.', 'Expedita sit velit blanditiis laboriosam rem. Sapiente nam assumenda pariatur voluptatibus. Laudantium aut architecto laborum quibusdam neque. Facere praesentium quasi cupiditate tenetur eum voluptate.', '2020-09-10 07:53:48', '2020-09-10 07:53:48', NULL, 'Professor'),
(54, 'Dr.', 'Numquam repellat molestiae qui quasi eveniet excepturi. Ipsum totam facilis fuga officiis voluptates adipisci rerum. Qui sapiente quos velit et illo est omnis et. Omnis possimus deleniti temporibus dolor ratione ad.', '2020-09-10 07:53:48', '2020-09-10 07:53:48', NULL, 'Buffing and Polishing Operator'),
(55, 'Dr.', 'Reprehenderit aspernatur quas neque voluptates doloribus dolores illo eos. Necessitatibus eos repudiandae similique quaerat vel. Aperiam natus consectetur distinctio voluptatibus animi tempora.', '2020-09-10 07:53:49', '2020-09-10 07:53:49', NULL, 'Public Health Social Worker'),
(56, 'Dr.', 'Et tempora dolores aperiam consequatur debitis sit. Sit alias corporis eos laboriosam sit. Atque est mollitia recusandae et. Aut impedit reiciendis doloremque sequi velit possimus iste. Molestias et facere sed ea nisi ut.', '2020-09-10 07:53:49', '2020-09-10 07:53:49', NULL, 'Travel Guide'),
(57, 'Prof.', 'Consectetur totam distinctio eaque in. Natus et quasi consequatur voluptatem magnam.', '2020-09-10 07:53:49', '2020-09-10 07:53:49', NULL, 'Sociologist'),
(58, 'Dr.', 'Repellat asperiores soluta perspiciatis qui reiciendis. Vel nihil ut eum reiciendis recusandae. Et id in necessitatibus commodi et aut quia. Laboriosam et esse et laboriosam dolor eaque ullam est.', '2020-09-10 07:53:49', '2020-09-10 07:53:49', NULL, 'Pharmacy Aide'),
(59, 'Prof.', 'Sit quis aut aut nihil. Non fugit non laudantium. Et rerum enim minima alias aliquid.', '2020-09-10 07:53:49', '2020-09-10 07:53:49', NULL, 'Credit Authorizer'),
(60, 'Miss', 'Debitis ipsum deleniti ad ratione qui fugit. Ut esse qui dolores enim sint est aut. Accusantium ut dolorum iusto aut consectetur id iste. Amet iure possimus dolores.', '2020-09-10 07:53:49', '2020-09-10 07:53:49', NULL, 'Receptionist and Information Clerk'),
(61, 'Ms.', 'Itaque eaque veniam doloribus et. Aliquam mollitia nihil sed earum qui ut. Magni consequatur commodi iste distinctio est possimus. Aut iusto cupiditate mollitia sit tenetur officiis ut.', '2020-09-10 07:53:49', '2020-09-10 07:53:49', NULL, 'First-Line Supervisor-Manager of Landscaping, Lawn Service, and Groundskeeping Worker'),
(62, 'Prof.', 'Molestiae unde culpa enim dolorum fugit cum. Provident aut cum sunt incidunt.', '2020-09-10 07:53:49', '2020-09-10 07:53:49', NULL, 'Fiber Product Cutting Machine Operator'),
(63, 'Prof.', 'Beatae sint reprehenderit fugiat ab ex. Aut perferendis nobis neque aperiam eum. Animi omnis deserunt quia omnis.', '2020-09-10 07:53:49', '2020-09-10 07:53:49', NULL, 'Commercial Diver'),
(64, 'Dr.', 'Alias aut et deserunt deserunt autem nobis provident. Placeat rerum qui quisquam est dolorem minus nostrum. Magni corrupti minima doloribus harum commodi. Cum quia fugiat consequatur sunt nam.', '2020-09-10 07:53:49', '2020-09-10 07:53:49', NULL, 'Insulation Worker'),
(65, 'Ms.', 'Sit repudiandae aliquid consequatur id iusto possimus. Pariatur aperiam sunt et mollitia nulla voluptates consectetur. Rem consequatur nisi quae quis molestiae dolores facere. Et sint commodi culpa laborum qui aliquid consectetur.', '2020-09-10 07:53:49', '2020-09-10 07:53:49', NULL, 'Budget Analyst'),
(66, 'Dr.', 'Nihil neque temporibus quo velit voluptas aperiam quaerat. Eos qui voluptas repellat maiores a est ipsum. Est est minus enim commodi laborum maiores.', '2020-09-10 07:53:49', '2020-09-10 07:53:49', NULL, 'Naval Architects'),
(67, 'Mrs.', 'Itaque possimus consectetur dolores iure qui. Et debitis sunt labore ipsam dolor. Ipsa dicta quia consequatur aperiam in temporibus alias.', '2020-09-10 07:53:49', '2020-09-10 07:53:49', NULL, 'Petroleum Pump System Operator'),
(68, 'Prof.', 'Harum dolore qui eius facere nihil. Maxime et est non ipsa dolorem consequatur. Ullam recusandae harum quia tempora eaque provident.', '2020-09-10 07:53:49', '2020-09-10 07:53:49', NULL, 'Athletic Trainer'),
(69, 'Mr.', 'Delectus commodi aut unde et consequatur dolorem. Voluptatem cum aliquid nemo incidunt sed. Blanditiis deleniti rerum harum qui.', '2020-09-10 07:53:49', '2020-09-10 07:53:49', NULL, 'License Clerk'),
(70, 'Dr.', 'Omnis impedit molestiae sapiente numquam optio nesciunt explicabo ea. Esse quia debitis odit possimus in ipsa officiis. Architecto accusantium non et ut nihil deleniti ipsum. Quo corrupti earum inventore est.', '2020-09-10 07:53:50', '2020-09-10 07:53:50', NULL, 'Operating Engineer'),
(71, 'Mrs.', 'Quia dolor quidem voluptas suscipit temporibus. Quia ea qui ut autem et ullam. Quaerat repellat consectetur voluptas consequatur tenetur voluptas. Est rerum provident dolor nulla cupiditate iste et minus.', '2020-09-10 07:53:50', '2020-09-10 07:53:50', NULL, 'Coating Machine Operator'),
(72, 'Mr.', 'Vel velit aut autem. Exercitationem harum non quia rerum. Culpa et aut in mollitia ipsa. Dolorem nostrum omnis magnam amet quam laborum.', '2020-09-10 07:53:50', '2020-09-10 07:53:50', NULL, 'Compacting Machine Operator'),
(73, 'Mr.', 'Omnis atque eum quaerat rem et molestiae corrupti. Et optio numquam et impedit tenetur deleniti sint quaerat. Consequuntur cumque illo explicabo qui quaerat dolor. In nemo est sit soluta veritatis eum.', '2020-09-10 07:53:50', '2020-09-10 07:53:50', NULL, 'Team Assembler'),
(74, 'Mrs.', 'Alias provident iusto voluptates molestias eum dignissimos. Corrupti voluptates sint iste temporibus sunt enim quo totam. Non dicta soluta natus in.', '2020-09-10 07:53:50', '2020-09-10 07:53:50', NULL, 'Motor Vehicle Inspector'),
(75, 'Miss', 'Reiciendis voluptatem officiis sed eveniet magnam itaque dignissimos. Consectetur dolorem mollitia repudiandae. Quae dolorum porro consequatur est voluptate mollitia quasi. Qui fugiat quibusdam cum ducimus.', '2020-09-10 07:53:50', '2020-09-10 07:53:50', NULL, 'Food Service Manager'),
(76, 'Miss', 'Aliquid aut aspernatur a exercitationem non voluptatem. Eum aperiam omnis fugiat inventore sit error.', '2020-09-10 07:53:50', '2020-09-10 07:53:50', NULL, 'Protective Service Worker'),
(77, 'Prof.', 'Ut ex ad quia cumque incidunt quae voluptatem. Corporis qui assumenda tempora tempora dignissimos. Rerum voluptatum doloremque soluta consequatur perspiciatis et minus possimus. Cupiditate architecto nulla et dolor.', '2020-09-10 07:53:50', '2020-09-10 07:53:50', NULL, 'Psychiatric Technician'),
(78, 'Prof.', 'Incidunt tenetur sed incidunt sunt. Nihil adipisci eius repellat rerum molestias molestiae. Accusamus repellat autem non eos ea voluptas et. Omnis impedit aliquam non vitae.', '2020-09-10 07:53:50', '2020-09-10 07:53:50', NULL, 'Gas Processing Plant Operator'),
(79, 'Prof.', 'Consequuntur ut non est nesciunt ex. Ut fugiat nulla autem consequatur maxime distinctio qui. Eaque corporis non magni dolore eveniet consectetur nisi. Similique dolorem magni enim libero molestiae mollitia dicta.', '2020-09-10 07:53:50', '2020-09-10 07:53:50', NULL, 'Medical Assistant'),
(80, 'Mr.', 'Magnam exercitationem consequatur sequi. Id natus eum maiores. Quis sed magni ipsam voluptate velit.', '2020-09-10 07:53:50', '2020-09-10 07:53:50', NULL, 'Chemist'),
(81, 'Mrs.', 'Facere amet ipsum non error modi quo. Est soluta aut eius voluptas voluptatem doloremque qui odio. Molestiae ut sit iste corporis.', '2020-09-10 07:53:50', '2020-09-10 07:53:50', NULL, 'Judge'),
(82, 'Prof.', 'Ut ea voluptas qui beatae nemo aut. Incidunt aut in aut minus. Laudantium quam iusto ut ea omnis.', '2020-09-10 07:53:50', '2020-09-10 07:53:50', NULL, 'Electrician'),
(83, 'Mr.', 'Consectetur aut velit ut veritatis dolore voluptas ab. Ut alias dolorem accusamus magnam ut ut.', '2020-09-10 07:53:50', '2020-09-10 07:53:50', NULL, 'Cooling and Freezing Equipment Operator'),
(84, 'Prof.', 'Molestiae repudiandae magnam ab fugit dolor provident explicabo laborum. Repellendus est omnis amet aliquam sed.', '2020-09-10 07:53:50', '2020-09-10 07:53:50', NULL, 'Ship Engineer'),
(85, 'Dr.', 'Ut possimus quod blanditiis itaque molestiae iure. Autem consequuntur hic temporibus earum fuga. Est veniam rerum minus id eum voluptas. Quod incidunt deserunt id dolorum ipsam.', '2020-09-10 07:53:50', '2020-09-10 07:53:50', NULL, 'Sales and Related Workers'),
(86, 'Prof.', 'Optio aut aut qui modi. Ut laborum quas ut dolores. Itaque amet quasi qui delectus amet voluptas et. Amet quia dignissimos laboriosam exercitationem repellat blanditiis fugit.', '2020-09-10 07:53:51', '2020-09-10 07:53:51', NULL, 'Music Composer'),
(87, 'Mrs.', 'Autem et sit dicta quaerat. Iure vitae enim exercitationem. Laboriosam quia et assumenda aspernatur omnis velit fugit sit.', '2020-09-10 07:53:51', '2020-09-10 07:53:51', NULL, 'Biological Technician'),
(88, 'Prof.', 'Qui in nisi quo est unde. Doloribus impedit ipsam quos laudantium aut atque. Qui doloribus quasi cum libero vero et qui.', '2020-09-10 07:53:51', '2020-09-10 07:53:51', NULL, 'Electrotyper'),
(89, 'Prof.', 'Minima deleniti est totam dicta asperiores. Et similique quis est sint mollitia. Aspernatur iste veniam ab ab at ut placeat.', '2020-09-10 07:53:51', '2020-09-10 07:53:51', NULL, 'Municipal Clerk'),
(90, 'Prof.', 'Deleniti quisquam voluptas dignissimos pariatur. Iure labore sit odio minus quo. Autem reprehenderit odit aperiam architecto.', '2020-09-10 07:53:51', '2020-09-10 07:53:51', NULL, 'Nursery Manager'),
(91, 'Dr.', 'Animi et et quod et. Libero inventore ab et minima porro qui velit. Minus et mollitia rerum. Excepturi amet in sint rerum provident perspiciatis vitae repellendus.', '2020-09-10 07:53:51', '2020-09-10 07:53:51', NULL, 'Bartender Helper'),
(92, 'Prof.', 'Et ut est dolorum sit placeat. Consequuntur nemo sed possimus adipisci non. Non fugit dolorem totam quia.', '2020-09-10 07:53:51', '2020-09-10 07:53:51', NULL, 'Upholsterer'),
(93, 'Dr.', 'Sed repellendus reiciendis cumque quisquam consequatur debitis quod. Unde vero ut sit a quis facere id eveniet. Quod et rerum totam enim est deserunt. Sit occaecati et harum at.', '2020-09-10 07:53:51', '2020-09-10 07:53:51', NULL, 'Municipal Fire Fighter'),
(94, 'Dr.', 'Distinctio quisquam tenetur libero ut. Culpa dolores qui corporis libero et.', '2020-09-10 07:53:51', '2020-09-10 07:53:51', NULL, 'Furnace Operator'),
(95, 'Mr.', 'Recusandae sunt omnis eum sint quod voluptas dolores voluptatem. Quam commodi nisi sint alias dolorem perferendis. Enim voluptas quas nulla at.', '2020-09-10 07:53:51', '2020-09-10 07:53:51', NULL, 'Home Entertainment Equipment Installer'),
(96, 'Dr.', 'Laborum deleniti inventore qui minus perferendis. Ut doloribus ut commodi iure ea consectetur quos. Minus officiis velit dicta dolorum soluta et. Et rerum et quia qui ut.', '2020-09-10 07:53:51', '2020-09-10 07:53:51', NULL, 'Health Services Manager'),
(97, 'Ms.', 'Ex temporibus iste facere perferendis velit voluptatem quisquam aspernatur. Hic quia provident et quam adipisci suscipit. Sunt placeat id atque quia aliquam rerum dolores. Ducimus qui animi autem unde impedit.', '2020-09-10 07:53:51', '2020-09-10 07:53:51', NULL, 'Personal Financial Advisor'),
(98, 'Miss', 'Fugit enim ad sunt non explicabo voluptatibus fugiat et. Deserunt mollitia aut necessitatibus vel soluta corporis non. Dolore qui qui ut non. Voluptas omnis dolor eius est culpa. Odio dolorem et eligendi id blanditiis qui.', '2020-09-10 07:53:51', '2020-09-10 07:53:51', NULL, 'Music Director'),
(99, 'Prof.', 'Dolores quidem similique ratione culpa quam est. Incidunt temporibus velit molestiae voluptas non soluta sed.', '2020-09-10 07:53:51', '2020-09-10 07:53:51', NULL, 'Gaming Supervisor'),
(100, 'Ms.', 'Debitis voluptatem quia aut nisi earum eligendi architecto. Ipsum nihil velit quasi deleniti. Nesciunt tempore quaerat maxime consequuntur magnam voluptatem. Ut aut nobis sed qui impedit debitis est vel.', '2020-09-10 07:53:52', '2020-09-10 07:53:52', NULL, 'Crushing Grinding Machine Operator'),
(101, 'Dr.', 'Qui molestiae maxime ex voluptas minus in. Nesciunt aut omnis hic unde ea consectetur. Perferendis qui sunt exercitationem animi quam.', '2020-09-10 07:53:52', '2020-09-10 07:53:52', NULL, 'Locomotive Engineer'),
(102, 'Dr.', 'Laudantium esse necessitatibus ut quasi eum tempore et. Hic in voluptatum exercitationem eos officiis. Unde ut dolorum laboriosam sed corrupti sit cupiditate vitae.', '2020-09-10 07:53:52', '2020-09-10 07:53:52', NULL, 'GED Teacher');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(31, '2014_10_12_000000_create_users_table', 1),
(32, '2014_10_12_100000_create_password_resets_table', 1),
(33, '2019_08_19_000000_create_failed_jobs_table', 1),
(34, '2020_09_10_131532_create_blog_table', 1),
(35, '2020_09_10_131907_add_category_in_blog_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
